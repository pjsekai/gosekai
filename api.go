package gosekai

import (
	"fmt"
	"net/http"
)

func (sk *Sekai) New() {
	body := sk.Post("/api/user", m{
		"platform":        "iOS",
		"deviceModel":     "iPad7,5",
		"operatingSystem": "iPadOS 17.0",
	})
	sk.UserId = body["userRegistration"].(m)["userId"]
	sk.Credential = body["credential"].(string)
}

func (sk *Sekai) Auth() {
	defer func() {
		if r := recover(); r != nil {
			if r, ok := r.(HTTPError); ok {
				if r.StatusCode == http.StatusNotAcceptable {
					sk.RuleAgreement()
					sk.Auth()
					return
				}
				panic(AuthError(r))
			}
			panic(r)
		}
	}()

	body := sk.Put(fmt.Sprintf("/api/user/%v/auth?refreshUpdatedResources=False", sk.UserId), m{
		"credential": sk.Credential,
	})

	for k, v := range body {
		if v, ok := v.(string); ok {
			sk.AppVersion[k] = v
		}
	}
	sk.SessionToken = body["sessionToken"].(string)
}

func (sk *Sekai) RuleAgreement() m {
	return sk.Post(fmt.Sprintf("/api/user/%v/rule-agreement", sk.UserId), m{
		"userId":     IdNumber(0),
		"credential": sk.Credential,
	})
}

func (sk *Sekai) Suite() {
	sk.Data = sk.Get(fmt.Sprintf("/api/suite/user/%v", sk.UserId))
}

func (sk *Sekai) Present() m {
	userPresents := sk.Data["userPresents"].(l)
	presentIds := make(l, len(userPresents))
	for i, present := range userPresents {
		presentIds[i] = present.(m)["presentId"]
	}

	if len(presentIds) == 0 {
		return nil
	}

	return sk.Post(fmt.Sprintf("/api/user/%v/present", sk.UserId), m{
		"presentIds": presentIds,
	})
}

func (sk *Sekai) Gacha(gachaId, gachaBehaviorId IdNumber) m {
	return sk.Put(fmt.Sprintf("/api/user/%v/gacha/%v/gachaBehaviorId/%v", sk.UserId, gachaId, gachaBehaviorId), nil)
}

func (sk *Sekai) Refresh(refreshableTypes ...string) m {
	if len(refreshableTypes) == 0 {
		refreshableTypes = []string{
			"convert_gacha_ceil_item",
			"new_pending_friend_request",
			"login_bonus",
			"user_report_thanks_message",
			"streaming_virtual_live_reward_status",
		}
	}

	return sk.Put(fmt.Sprintf("/api/user/%v/home/refresh", sk.UserId), m{
		"refreshableTypes": refreshableTypes,
	})
}

func (sk *Sekai) LoginBonus() m {
	return sk.Refresh()
}

func (sk *Sekai) Inherit(password string) m {
	return sk.Put(fmt.Sprintf("/api/user/%v/inherit", sk.UserId), m{
		"password": password,
	})
}
