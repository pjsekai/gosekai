package gosekai

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strings"
	"sync"

	"github.com/google/uuid"
	"github.com/vmihailenco/msgpack/v5"
	"golang.org/x/time/rate"
)

type l = []any
type m = map[string]any
type IdNumber any

type Sekai struct {
	*Config

	UserId       IdNumber
	Credential   string
	SessionToken string
	Data         m

	installId string
	kc        string
	cookie    string
}

type Config struct {
	Domain     string
	AppVersion map[string]string
	*Database

	wg *sync.WaitGroup
	rl *rate.Limiter

	*Crypt
	*http.Client
}

func (sk *Sekai) requestWithoutLock(method string, url string, body m) m {
	if strings.HasPrefix(url, "/") {
		url = "https://" + sk.Domain + url
	}

	// encrypt the data
	var data []byte
	var err error
	if body == nil {
		if method != "GET" {
			data = sk.encrypt([]byte{})
		}
	} else {
		data, err = msgpack.Marshal(body)
		assert(err)
		data = sk.encrypt(data)
	}

	// create the request
	req, err := http.NewRequest(method, url, bytes.NewBuffer(data))
	assert(err)

	// make headers
	req.Header.Add("accept", "application/octet-stream")
	req.Header.Add("content-type", "application/octet-stream")

	req.Header.Add("x-ai", "")
	req.Header.Add("x-ga", "")
	req.Header.Add("x-ma", "")
	req.Header.Add("x-kc", sk.kc)
	req.Header.Add("x-if", "")

	req.Header.Add("x-devicemodel", "iPad12,1")
	req.Header.Add("x-operatingsystem", "iPadOS 17.0")
	req.Header.Add("x-platform", "iOS")
	req.Header.Add("user-agent", "ProductName/211 CFNetwork/1568.100.1.2.1 Darwin/24.0.0")

	req.Header.Add("x-unity-version", "2022.3.21f1")
	req.Header.Add("x-app-hash", sk.AppVersion["appHash"])
	req.Header.Add("x-app-version", sk.AppVersion["appVersion"])
	req.Header.Add("x-asset-version", sk.AppVersion["assetVersion"])
	req.Header.Add("x-data-version", sk.AppVersion["dataVersion"])

	req.Header.Add("x-install-id", sk.installId)
	req.Header.Add("x-request-id", uuid.NewString())

	// add session token and cookie if there is
	if sk.SessionToken != "" {
		req.Header.Add("x-session-token", sk.SessionToken)
	}

	if sk.cookie != "" {
		req.Header.Add("cookie", sk.cookie)
	}

	// do the request and read status
	resp, err := sk.Client.Do(req)
	assert(err)
	defer resp.Body.Close()

	// read the body
	data, err = io.ReadAll(resp.Body)
	assert(err)

	// error handling
	switch resp.StatusCode {
	case http.StatusOK:
		// pass

	case http.StatusForbidden:
		if bytes.Contains(data, []byte("Request blocked.")) {
			go sk.rl.WaitN(context.Background(), 5)
			assert(TooManyRequests(HTTPError{resp.StatusCode}))
		}
		assert(HTTPError{resp.StatusCode})

	case http.StatusUpgradeRequired:
		go sk.update()
		assert(HTTPError{resp.StatusCode})

	case http.StatusServiceUnavailable:
		assert(ServerMaintenance(HTTPError{resp.StatusCode}))

	case http.StatusTooManyRequests:
		go sk.rl.WaitN(context.Background(), 5)
		assert(TooManyRequests(HTTPError{resp.StatusCode}))

	default:
		assert(HTTPError{resp.StatusCode})
	}

	// read the new session token and cookie if there is
	if st := resp.Header.Get("x-session-token"); st != "" {
		sk.SessionToken = st
	}
	if sck := resp.Header.Get("set-cookie"); sck != "" {
		sk.cookie = sck
	}

	// return json directly
	if resp.Header.Get("Content-Type") == "application/json; charset=utf-8" {
		var body m
		json.Unmarshal(data, &body)
		return body
	}

	// decrypt
	data = sk.decrypt(data)
	if len(data) == 0 {
		return nil
	}
	err = msgpack.Unmarshal(data, &body)
	assert(err)

	// update sk clinet data
	if ur, ok := body["updatedResources"]; ok {
		for k, v := range ur.(m) {
			sk.Data[k] = v
		}
	}
	return body
}

func getVersion() map[string]string {
	req, err := http.NewRequest("GET", "https://version.pjsekai.moe/jp.json", nil)
	assert(err)

	resp, err := http.DefaultClient.Do(req)
	assert(err)
	defer resp.Body.Close()

	data, err := io.ReadAll(resp.Body)
	assert(err)

	var version map[string]string
	err = json.Unmarshal(data, &version)
	assert(err)

	return version
}

func (sk *Sekai) update() {
	log.Println("start to update sekai app version")

	sk.wg.Add(1)
	defer sk.wg.Done()

	version := getVersion()
	for k, v := range version {
		sk.AppVersion[k] = v
	}

	body := sk.requestWithoutLock("GET", "/api/system", nil)
	for _, av := range body["appVersions"].(l) {
		av := av.(m)
		if av["appVersionStatus"] == "available" && av["appVersion"] == sk.AppVersion["appVersion"] {
			for k, v := range av {
				sk.AppVersion[k] = v.(string)
			}

			log.Printf("sekai app version: %v\n", sk.AppVersion)
			return
		}
	}

	for _, av := range body["appVersions"].(l) {
		av := av.(m)
		if av["appVersionStatus"] == "available" {
			for k, v := range av {
				sk.AppVersion[k] = v.(string)
			}

			log.Printf("sekai app version: %v\n", sk.AppVersion)
			return
		}
	}

	// sk.Database = NewDatabase(sk.requestWithoutLock("GET", "/api/suite/master", nil))
	// log.Printf("sekai database updated")
}

func (sk *Sekai) Request(method string, url string, body m) m {
	sk.wg.Wait()
	sk.rl.Wait(context.Background())

	return sk.requestWithoutLock(method, url, body)
}

func (sk *Sekai) Get(url string) m {
	return sk.Request("GET", url, nil)
}

func (sk *Sekai) Post(url string, body m) m {
	return sk.Request("POST", url, body)
}

func (sk *Sekai) Patch(url string, body m) m {
	return sk.Request("PATCH", url, body)
}

func (sk *Sekai) Put(url string, body m) m {
	return sk.Request("PUT", url, body)
}

func (sk *Sekai) Delete(url string, body m) m {
	return sk.Request("DELETE", url, body)
}
