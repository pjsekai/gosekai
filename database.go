package gosekai

import (
	"fmt"
	"strings"
	"sync"
)

type Database struct {
	collections map[string]*Collection
}

type Collection struct {
	data               l
	indexedCollections map[string]*IndexedCollection
	lock               sync.Mutex
}

type IndexedCollection struct {
	data m
	keys []string
}

func NewDatabase(data m) *Database {
	collections := make(map[string]*Collection)
	for k, v := range data {
		collections[k] = NewCollection(v.(l))
	}
	return &Database{collections: collections}
}

func NewCollection(data l) *Collection {
	return &Collection{
		data:               data,
		indexedCollections: make(map[string]*IndexedCollection),
	}
}

func (db *Database) Get(name string) *Collection {
	return db.collections[name]
}

func (c *Collection) Key(keys ...string) *IndexedCollection {
	c.lock.Lock()
	defer c.lock.Unlock()

	key := formatKeys(keys)
	if indexedCollection, ok := c.indexedCollections[key]; ok {
		return indexedCollection
	}
	indexedCollection := &IndexedCollection{
		data: toIndexedCollection(c.data, keys...),
		keys: keys,
	}
	c.indexedCollections[key] = indexedCollection
	return indexedCollection
}

func (ic *IndexedCollection) Value(values ...string) m {
	result := ic.data
	for _, value := range values {
		result = result[value].(m)
	}
	return result
}

func formatKeys(keys []string) string {
	return strings.Join(keys, "-")
}

func toIndexedCollection(collection l, keys ...string) m {
	collectionGroup := make(map[string]l)

	for _, v := range collection {
		k := fmt.Sprint(v.(m)[keys[0]])
		if _, ok := collectionGroup[k]; !ok {
			collectionGroup[k] = make(l, 0)
		}
		collectionGroup[k] = append(collectionGroup[k], v)
	}

	indexedCollection := make(m)

	if len(keys) > 1 {
		for k, v := range collectionGroup {
			indexedCollection[k] = toIndexedCollection(v, keys[1:]...)
		}
	} else {
		for k, v := range collectionGroup {
			indexedCollection[k] = v[0]
		}
	}

	return indexedCollection
}
