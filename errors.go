package gosekai

import (
	"fmt"
	"log"
	"runtime/debug"
)

type HTTPError struct {
	StatusCode int
}

func (e HTTPError) Error() string {
	return fmt.Sprintf("HTTP error %v", e.StatusCode)
}

type TooManyRequests HTTPError

func (e TooManyRequests) Error() string {
	return "too many requests: " + HTTPError(e).Error()
}

type ServerMaintenance HTTPError

func (e ServerMaintenance) Error() string {
	return "maintenance: " + HTTPError(e).Error()
}

type AuthError HTTPError

func (e AuthError) Error() string {
	return "auth error: " + HTTPError(e).Error()
}

func assert(err error) {
	if err != nil {
		log.Println(err)
		debug.PrintStack()
		panic(err)
	}
}
