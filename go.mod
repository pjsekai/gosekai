module gitlab.com/pjsekai/gosekai

go 1.20

require (
	github.com/google/uuid v1.4.0
	github.com/vmihailenco/msgpack/v5 v5.4.1
	golang.org/x/time v0.4.0
)

require github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
