package gosekai

import (
	"encoding/hex"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/google/uuid"
	"golang.org/x/time/rate"
)

var enKey, _ = hex.DecodeString(os.Getenv("PJSEKAI_EN_KEY"))
var enIV, _ = hex.DecodeString(os.Getenv("PJSEKAI_EN_IV"))

var configEN = Config{
	Domain:     "n-production-game-api.sekai-en.com",
	AppVersion: make(map[string]string),

	wg: &sync.WaitGroup{},
	rl: rate.NewLimiter(0.75, 1),

	Crypt: &Crypt{
		key: enKey,
		iv:  enIV,
		jwt: []byte(os.Getenv("PJSEKAI_JWT")),
	},
	Client: &http.Client{
		Timeout: 60 * time.Second,
		Transport: &http.Transport{
			MaxIdleConns:        64,
			MaxIdleConnsPerHost: 64,
			IdleConnTimeout:     60 * time.Second,
		},
	},
}

var mutexNewSekaiEN = new(sync.Mutex)

// NewSekaiEN creates a SekaiEN client, but not a SekaiEN account.
func NewSekaiEN() *Sekai {
	mutexNewSekaiEN.Lock()
	defer mutexNewSekaiEN.Unlock()

	sk := &Sekai{
		Config: &configEN,
		Data:   make(m),

		installId: uuid.NewString(),
		kc:        uuid.NewString(),
	}
	if len(sk.AppVersion) == 0 {
		sk.update()
	}
	return sk
}

// NewSekaiENFromCredential creates a SekaiEN client, and auths with the given credential.
func NewSekaiENFromCredential(userId IdNumber, credential string) *Sekai {
	sk := NewSekaiEN()
	sk.UserId = userId
	sk.Credential = credential
	sk.Auth()
	return sk
}
