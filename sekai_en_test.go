package gosekai

import (
	"fmt"
	"testing"
)

func TestSekaiEN(t *testing.T) {
	sk := NewSekaiEN()
	sk.New()
	sk.Auth()
	sk.Suite()
	keys := make([]string, 0, len(sk.Data))
	for k := range sk.Data {
		keys = append(keys, k)
	}
	fmt.Printf("keys: %v\n", keys)
}
