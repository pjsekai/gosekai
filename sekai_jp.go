package gosekai

import (
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/google/uuid"
	"golang.org/x/time/rate"
)

var configJP = Config{
	Domain:     "production-game-api.sekai.colorfulpalette.org",
	AppVersion: make(map[string]string),

	wg: &sync.WaitGroup{},
	rl: rate.NewLimiter(64, 64),

	Crypt: &Crypt{
		key: []byte(os.Getenv("PJSEKAI_KEY")),
		iv:  []byte(os.Getenv("PJSEKAI_IV")),
		jwt: []byte(os.Getenv("PJSEKAI_JWT")),
	},
	Client: &http.Client{
		Timeout: 60 * time.Second,
		Transport: &http.Transport{
			MaxIdleConns:        64,
			MaxIdleConnsPerHost: 64,
			IdleConnTimeout:     60 * time.Second,
		},
	},
}

var mutexNewSekaiJP = new(sync.Mutex)

// NewSekaiJP creates a SekaiJP client, but not a SekaiJP account.
func NewSekaiJP() *Sekai {
	mutexNewSekaiJP.Lock()
	defer mutexNewSekaiJP.Unlock()

	sk := &Sekai{
		Config: &configJP,
		Data:   make(m),

		installId: uuid.NewString(),
		kc:        uuid.NewString(),
	}
	sk.Post("https://issue.sekai.colorfulpalette.org/api/signature", nil)
	if len(sk.AppVersion) == 0 {
		sk.update()
	}
	return sk
}

// NewSekaiJPFromCredential creates a SekaiJP client, and auths with the given credential.
func NewSekaiJPFromCredential(userId IdNumber, credential string) *Sekai {
	sk := NewSekaiJP()
	sk.UserId = userId
	sk.Credential = credential
	sk.Auth()
	return sk
}
