package gosekai

import (
	"fmt"
	"testing"
)

func TestSekaiJP(t *testing.T) {
	sk := NewSekaiJP()
	sk.New()
	sk.Auth()
	sk.Suite()
	sk.LoginBonus()
}

func TestSekaiDatabase(t *testing.T) {
	sk := NewSekaiJP()

	fmt.Println(sk.Database.Get("musics").Key("id").Value("400"))
	fmt.Println(sk.Database.Get("musicDifficulties").Key("musicId", "musicDifficulty").Value("400", "master"))
}
